import numpy as np
import time
import sys
import csv

if len(sys.argv) != 3:
    print ("usage: python gaussian.py <NUM POINTS> <FILENAME>")
    sys.exit(0)

points = 0

numPoints = int(sys.argv[1])
filename = sys.argv[2]

domains = [[0, 10], [0, 10]]

domainMeans = [(7, 2), (2, 7), (2, 2)]
domainCov = [[2, 0], [0, 2]]

valMeans = [-2, 0, 2]  # mean and standard deviation
valStdDev = [1, 2, 1]  # mean and standard deviation

fieldnames = ['x', 'y', 'z', 't']

with open(filename, 'wb') as csvfile:
    writer = csv.DictWriter(csvfile, fieldnames = fieldnames, lineterminator='\n')
    writer.writeheader()

    for sampleIndex in xrange(3):
        sample = np.random.multivariate_normal(
            domainMeans[sampleIndex], domainCov, numPoints)
        val = np.random.normal(
            valMeans[sampleIndex], valStdDev[sampleIndex], numPoints)
        numSamples = sample.shape[0]

        for i in xrange(numSamples):
            if 0 <= sample[i, 0] and sample[i, 0] <= 10 and 0 <= sample[i, 1] and sample[i, 1] <= 10:
                writer.writerow({"x": sample[i, 0], "y": sample[i, 1], "z": val[i], "t": time.asctime(time.gmtime(i))})
                points += 1
                if points == numPoints:
                    csvfile.close()
                    sys.exit(0)
